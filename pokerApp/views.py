from django.shortcuts import render
from django.views import View
from pokerApp.models import YourClass


# Create your views here.
class Home(View):
    def get(self, request):
        return render(request, 'pokerApp/index.html')

    def post(self, request):
        your_instance = YourClass()
        command_input = request.POST["command"]
        user_name = request.POST["user"]
        if command_input:
            response = your_instance.command(user_name + ":" + command_input)
        else:
            response = ""
        return render(request, 'pokerApp/index.html', {"message": response})
