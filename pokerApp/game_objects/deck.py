import pokerApp.game_objects.card


class Deck:
    def __init__(self):
        # create a standard 52 card deck of cards
        # deck of cards is initialized as 1 - 13 rank all in suit 1, followed by suits 2 - 4
        pass

    def count(self):
        # return the number of cards remaining in the deck
        pass

    def draw(self):
        # return and remove the top card in the deck
        # if the deck is empty, raise a ValueError
        pass

    def shuffle(self):
        # shuffle the deck using a random number generator
        pass
