from django.test import TestCase
import pokerApp


# Done By Shawn
# As A Player I want to see my cards so I can strategize
class TestDisplayCards(TestCase):
    def test_no_cards(self):
        player = []
        with self.assertRaises(ValueError):
            self.app.command("display_hand")

    def test_has_cards(self):
        player = ['6S', '4H', '5D']
        self.assertEquals(self.app.command("display_hand"), "6S, 4H, 5D")


"""As an admin I want to create player accounts so players can play poker."""


class CreatePlayerAccount(TestCase):
    # assume admin created an account "user1"
    def test_account_exist(self):
        self.assertEqual(pokerApp.command(), "user1")


"""As an admin I want to have a special username 
   so I can differentiate from the players."""


class AdminSpecialUsername(TestCase):
    pass


# As a player I want to see my current chip count so I know whether I can keep playing.
# Author: Alex Traub
class TestPlayerCurrentChipCount(TestCase):
    # Assume you are player with username "player1" with 150 chips

    def test_correct_chip_count(self):
        self.assertEqual(pokerApp.command(), 150)

    def test_current_chip_count_greater_than_0(self):
        self.assertGreater(pokerApp.command(), 0)


# As a player I want to be removed from the game if I have no chips remaining.
# Author: Noah Schrader
class PlayerWithNoChips(TestCase):
    # assume player with username "player1" has no chips
    def test_player_removed(self):
        self.assertEqual(pokerApp.command(), 0)
        self.assertTrue(pokerApp.command())


# As I player I want to see the amount needed to call so that I can see if I want to keep playing.
# Author: Colin Stolfa
class AmountToCall(TestCase):
    def test_call_bet(self):
        # assume you are player1 and player2 just bet 5 chips
        self.assertEqual(pokerApp.comand(), 5)

    def test_call_no_bet(self):
        # assume there is no current bet
        self.assertIsNone(pokerApp.comand())
