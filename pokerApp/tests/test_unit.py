from django.test import TestCase
from unittest import mock
import pokerApp.game_objects.card
from pokerApp.game_objects.card import Card
from pokerApp.game_objects.deck import Deck


# Card tests
class CardInitCorrectRankAndSuit(TestCase):
    def test_standard_deck(self):
        for i in range(1, 5):
            for j in range(1, 14):
                card = Card(i, j)
                self.assertTrue(card.getSuit() == i)
                self.assertTrue(card.getRank() == j)


class CardInitRaiseValueError(TestCase):
    def test_invalid_suit(self):
        with self.assertRaises(ValueError):
            Card(0, 1)
        with self.assertRaises(ValueError):
            Card(5, 1)
        with self.assertRaises(ValueError):
            Card("wrong type", 0)
        with self.assertRaises(ValueError):
            Card(1.1, 0)

    def test_invalid_rank(self):
        with self.assertRaises(ValueError):
            Card(1, 0)
        with self.assertRaises(ValueError):
            Card(1, 14)
        with self.assertRaises(ValueError):
            Card(0, "wrong type")
        with self.assertRaises(ValueError):
            Card(0, 1.1)


class CardGetSuit(TestCase):
    """
    testing suit of every possible value to see if it returns the correct suit
    """

    def test_get_suit(self):
        for i in range(1, 5):
            for j in range(1, 14):
                c = Card(i, j)
                self.assertEquals(c.getSuit(), i)


class CardGetRank(TestCase):
    # test individual cases without loops
    def test_valid_rank01(self):
        card = Card(1, 1)
        self.assertEqual(1, card.getRank())

        card2 = Card(2, 3)
        self.assertEqual(3, self.getRank())

    # test with only rank in loop
    def test_valid_rank02(self):
        suit = 3
        for rank in range(1, 14):
            card = Card(suit, rank)
            self.assertEqual(rank, card.getRank())

    """ 
    this test tests all the valid suit/rank combinations
    and ensures that getRank returns the correct
    integer value of the rank 
    """

    def test_all_valid_ranks(self):
        for suit in range(1, 5):
            for rank in range(1, 14):
                card = Card(suit, rank)
                self.assertEqual(rank, card.getRank())


class CardGetValue(TestCase):
    """
    Checks that all possible 52 cards have the correct value.
    As no game is currently specified, we can assume any card has a value of 0.
    """

    # Initializes a list of Card objects, 1 for each suit and rank combination
    def setUp(self):
        self.cards = []

        for suit in range(1, 5):
            for rank in range(1, 14):
                self.cards.append(Card(suit, rank))

    def test_card_values(self):
        for card in self.cards:
            with self.subTest():
                self.assertEqual(0,
                                 card.getValue(),
                                 "Incorrect card value")


class CardStr(TestCase):
    """
    tests card.__str__() with every value 1-13 rank adn 1-4 suit
    """

    def test_str(self):
        # iterates through all cases of cards
        for s in range(1, 5):  # all suits
            for r in range(1, 14):  # all ranks
                # creates sting to check against __str__ function
                string = ""
                if r == 1:
                    string += "A"
                elif r == 11:
                    string += "J"
                elif r == 12:
                    string += "Q"
                elif r == 13:
                    string += "K"
                else:
                    string += f"{r}"
                if s == 1:
                    string += "C"
                elif s == 2:
                    string += "S"
                elif s == 3:
                    string += "H"
                else:
                    string += "D"

                # assertion check on current card
                self.assertEquals(Card(r, s).__str__(), string)


# deck tests
class DeckInit(TestCase):
    """@mock.patch('card.Card') replaces any instance of the Card class with
        a MagicMock"""

    @mock.patch('pokerApp.game_objects.card.Card')
    def test_standard_deck(self, mock_card):
        deck = Deck()
        self.assertEqual(deck.count(), 52)
        for i in range(1, 5):
            for j in range(1, 14):
                mock_card = pokerApp.game_objects.card.Card(deck.draw())
                self.assertEqual(mock_card.getSuit(), i)
                self.assertEqual(mock_card.getRank(), j)


class DeckCount(TestCase):
    """
    drawing card from the deck and testing if each removal matches the count method return
    """

    def test_deck_count(self):
        d = Deck()
        for i in range(1, 53):
            d.draw()
            self.assertEquals(d.count(), 52 - i)


class DeckDrawValue(TestCase):
    """
    Tests that a non-empty Deck can draw cards
    """

    # Tests that only Card objects are drawn from a Deck
    def test_draws_cards(self):
        deck = Deck()
        for i in range(0, 53):
            with self.subTest(f"Draw {i}"):
                self.assertIsInstance(deck.draw(), Card)


class DeckDrawException(TestCase):
    """
    this test tests drawing from an empty deck.
    we start by initializing a deck (d) and drawing
    52 cards from it. Then we attempt to draw 1 card
    from the now empty deck which should raise a
    ValueError
    """

    def test_empty_deck(self):
        d = Deck()
        for x in range(52):
            d.draw()
        with self.assertRaises(ValueError):
            d.draw()


class DeckShuffle(TestCase):
    """
    assume that a deck of cards is initialized as 1 - 13 rank all in suit 1, followed by suits 2 - 4
    tests every card in a deck to see if the deck is shuffled
    test an empty deck shuffle throws a TypeError
    """

    def setUp(self):
        self.cards = []
        # iterate through each suit
        for s in range(1, 5):
            # iterate through each rank within suit
            for r in range(1, 14):
                # create a mock card with mock card functions
                fake_card = mock.Mock(pokerApp.game_objects.card.Card)
                fake_card.getSuit = mock.Mock(value=s)
                fake_card.getRank = mock.Mock(value=r)
                # add mock card to cards
                self.cards.append(fake_card)

    @mock.patch('pokerApp.game_objects.card.Card')
    def test_shuffle(self, mock_card):
        self.d = Deck()
        # shuffle d
        self.d.shuffle()
        # init count to 0 for count how many cards in same place
        count = 0
        # iterate through all cards in list of cards
        for c in self.cards:
            # take top card and make a mock instance of it
            mock_card = pokerApp.game_objects.card.Card(self.d.draw())
            # evaluate if card rank and suit equivalent to cards in list
            if c.getSuit.value == mock_card.getSuit() and c.getRank.value == mock_card.getRank():
                # increment count when match found
                count += 1

        # checks if count < 52
        self.assertLess(count, 52)

    def test_empty_shuffle(self):
        self.d = Deck()
        # empty deck
        for i in range(52):
            self.d.draw()
        # assert type error for empty deck when shuffled
        with self.assertRaises(TypeError):
            self.d.shuffle()